const express = require("express");
const cors = require("cors");

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors()); //! DOC: https://github.com/expressjs/cors#readme
app.use(require("sanitize").middleware); //! DOC: https://github.com/pocketly/node-sanitize#readme

const { PORT } = require("./db/config");

//! routes
require("./routers")(app);

// error handler
app.use((err, req, res, next) => {
  res.status(200).json({
    status: err.status || 500,
    message: err.message,
  });
});

// Catch all other routes
app.use("*", (req, res) => {
  res.status(200).json({
    status: 404,
    message: "Path Not found",
  });
});

app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});
