const { CustomError } = require("../utils");
const DB_QUERY = require("../db");
module.exports = {
  async getUsers(req, res, next) {
    try {
      let data = await DB_QUERY("SELECT * FROM `users`");
      res.status(200).json({
        status: 200,
        data,
      });
    } catch (error) {
      return next(new CustomError(error.message, 500));
    }
  },
  async newUser(req, res, next) {
    try {
      let name = req.bodyString("name");
      let result = await DB_QUERY("INSERT INTO `users`(`name`) VALUES (?)", [
        name,
      ]);

      res.status(200).json({
        status: 200,
        result,
      });
    } catch (error) {
      return next(new CustomError(error.message, 500));
    }
  },
  async updateUser(req, res, next) {
    try {
      let id = req.bodyInt("id");
      let name = req.bodyString("name");
      let result = await DB_QUERY(
        "UPDATE `users` SET `name` = ? WHERE `id` = ?",
        [name, id]
      );

      res.status(200).json({
        status: 200,
        result,
      });
    } catch (error) {
      return next(new CustomError(error.message, 500));
    }
  },
  async deleteUser(req, res, next) {
    try {
      let id = req.bodyInt("id");
      let result = await DB_QUERY("DELETE FROM `users` WHERE `id` = ?", [id]);

      res.status(200).json({
        status: 200,
        result,
      });
    } catch (error) {
      return next(new CustomError(error.message, 500));
    }
  },
};
