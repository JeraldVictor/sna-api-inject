const express = require("express");
const app = express.Router();
const controller = require("../controllers");
const securedController = require("../controllers/secured");

app.get("/", controller.home);
app.post("/", controller.newUser);
app.put("/", controller.updateUser);
app.delete("/", controller.deleteUser);

app.get("/secured", securedController.getUsers);
app.post("/secured", securedController.newUser);
app.put("/secured", securedController.updateUser);
app.delete("/secured", securedController.deleteUser);

module.exports = app;
