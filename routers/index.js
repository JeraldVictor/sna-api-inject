const HomeRoutes = require("./home");

module.exports = (app) => {
  app.use("/", HomeRoutes);
};
