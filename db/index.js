const mysql = require("mysql");
const { db } = require("./config");
const util = require("util");
const connection = mysql.createPool(db);
module.exports = util.promisify(connection.query).bind(connection);
